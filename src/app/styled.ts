import { styled } from '@mui/material';

export const Container = styled('div')(() => ({
  display: 'grid',
  gridTemplateColumns: '75px auto',
  width: '100vw',
  height: '100vh',
}));
