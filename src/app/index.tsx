// import Navigation from 'bms_navigation/app';
import { FC, lazy, Suspense } from 'react';
import { ErrorBoundary } from 'bms-shared/src';
import { CircularProgress } from '@mui/material';
import * as Styled from './styled';

// const Navigation = lazy(() =>
//   import('bms_navigation/app').catch(() => import('./ErrorMessage'))
// );

const Navigation = lazy(() =>
  import('bms_navigation/app').catch(() => import('./ErrorMessage'))
);

const Remote = ({ component }) => (
  <ErrorBoundary>
    <Suspense fallback={<CircularProgress />}>{component}</Suspense>
  </ErrorBoundary>
);

const App = () => (
  <Styled.Container>
    <Remote component={<Navigation />} />

    <div>root</div>
  </Styled.Container>
);

export default App;
