import React, { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { CssBaseline } from '@mui/material';
import App from '@app/index';

const container = document.getElementById('app');
const root = createRoot(container!);
root.render(
  <StrictMode>
    <CssBaseline />
    <App />
  </StrictMode>
);
