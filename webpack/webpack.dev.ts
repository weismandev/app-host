import webpack from 'webpack';
import { merge } from 'webpack-merge';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import process from 'process';
import paths from './paths';
import common from './webpack.common';

import 'webpack-dev-server';

const devConfig: webpack.Configuration = merge(common, {
  mode: 'development',
  output: {
    path: paths.build,
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: 'auto',
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: 'src/index.html',
      excludeChunks: [`${process.env.MODULE}`],
    }),
  ],
  devtool: 'eval-source-map',
  performance: {
    hints: false,
  },
  devServer: {
    historyApiFallback: true,
    open: true,
    hot: true,
    port: 3001,
  },
});

export default devConfig;
