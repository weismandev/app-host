import webpack from 'webpack';
import dotenv from 'dotenv';
import { dependencies } from '../package.json';
import paths from './paths';

dotenv.config();

const config: webpack.Configuration = {
  entry: paths.entry,
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        loader: 'esbuild-loader',
        options: {
          target: 'esnext',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[contenthash].[ext]',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.mp3$/,
        loader: 'file-loader',
        options: {
          name: 'sound/[name].[contenthash].[ext]',
        },
      },
    ],
  },
  optimization: {
    splitChunks: false,
  },
  plugins: [
    new webpack.container.ModuleFederationPlugin({
      name: 'bms-host',
      filename: 'remoteEntry.js',
      remotes: {
        bms_navigation: `bms_navigation@${process.env.BMS_NAVIGATION_URL}/remoteEntry.js`,
      },
      shared: {
        react: {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        'react-dom': {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        effector: {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        'effector-react': {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        patronum: {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        '@mui/material': {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        '@emotion/react': {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        '@emotion/styled': {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
        i18next: {
          singleton: true,
          eager: true,
          requiredVersion: 'auto',
        },
      },
    }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.mjs'],
    alias: {
      ...paths,
    },
  },
  target: 'web',
};

export default config;
