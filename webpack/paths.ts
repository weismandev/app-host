import * as path from 'path';

const paths = {
  src: path.resolve(__dirname, '../src'),
  entry: path.resolve(__dirname, '../src/index.ts'),
  build: path.resolve(__dirname, '../build'),
  '@app': path.resolve(__dirname, '../src/app'),
  '@entities': path.resolve(__dirname, '../src/entities'),
  '@features': path.resolve(__dirname, '../src/features'),
  '@pages': path.resolve(__dirname, '../src/pages'),
  '@shared': path.resolve(__dirname, '../src/shared'),
  '@widgets': path.resolve(__dirname, '../src/widget'),
};

export default paths;
